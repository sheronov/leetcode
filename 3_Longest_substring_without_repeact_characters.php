<?php

class Solution
{

    /**
     * @param String $s
     *
     * @return Integer
     */
    function lengthOfLongestSubstring($s)
    {
        $length = strlen($s);
        $letters = $tempLetters = [];

        for ($i = 0; $i < $length; $i++) {
            if (in_array($s[$i], $tempLetters, true)) {
                if (count($tempLetters) > count($letters)) {
                    $letters = $tempLetters;
                }
                foreach ($tempLetters as $j => $letter) {
                    if ($s[$i] === $letter) {
                        unset($tempLetters[$j]);
                        break;
                    }
                    unset($tempLetters[$j]);
                }
            }
            $tempLetters[] = $s[$i];
        }

        if (count($tempLetters) > count($letters)) {
            $letters = $tempLetters;
        }

        return count($letters);

    }
}

$solution = new Solution();
echo $solution->lengthOfLongestSubstring('abcabcbb');