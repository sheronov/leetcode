<?php

class Solution
{

    /**
     * @param Integer[] $nums
     * @param Integer   $target
     *
     * @return Integer[]
     */
    function twoSum($nums, $target)
    {
        $indices = [];
        while (!$indices) {
            $indices = $this->findIndeces($nums, $target);
        }

        return $indices;
    }

    /** MY RECURSIVE METHOD (MIDDLE TIME, MIDDLE MEMORY) */
    function findIndeces(&$nums, $target)
    {
        $lastVal = array_pop($nums);
        $lastIndex = count($nums);
        foreach ($nums as $i => $iValue) {
            if ($iValue + $lastVal === $target) {
                return [$i, $lastIndex];
            }
        }

        return false;
    }


    /** LOW MEMORY METHOD */
    function leetCodeSolutionBrutForce($nums, $target)
    {
        $count = count($nums);
        for ($i = 0; $i < $count; $i++) {
            for ($j = $i + 1; $j < $count; $j++) {
                if ($nums[$i] + $nums[$j] === $target) {
                    return [$i, $j];
                }
            }
        }
    }


    /** TWO PASS METHOD - doesn't work with tho same numbers in array */
    function leetCodeSolutionTwoPassHashTable($nums, $target)
    {
        $count = count($nums);
        $map = [];
        for ($i = 0; $i < $count; $i++) {
            $map[$nums[$i]] = $i;
        }

        for ($i = 0; $i < $count; $i++) {
            $complement = $target - $nums[$i];
            if (isset($map[$complement]) && $map[$complement] !== $i) {
                return [$i, $map[$complement]];
            }
        }
    }

    /** SUPER FAST METHOD */
    function leetCodeSolutionOnePassHashTable($nums, $target)
    {
        $count = count($nums);
        $map = [];
        for ($i = 0; $i < $count; $i++) {
            $complement = $target - $nums[$i];
            if (isset($map[$complement])) {
                return [$map[$complement], $i];
            }
            $map[$nums[$i]] = $i;
        }
    }
}

$task = new Solution();
print_r($task->twoSum([0, 4, 3, 0], 0));