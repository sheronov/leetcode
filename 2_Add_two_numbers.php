<?php

/**
 * Definition for a singly-linked list.
 */
class ListNode
{
    public $val  = 0;
    public $next = null;

    function __construct($val)
    {
        $this->val = $val;
    }
}

class Solution
{

    /**
     * @param ListNode $l1
     * @param ListNode $l2
     *
     * @return ListNode
     */
    function addTwoNumbers(ListNode $l1, ListNode $l2)
    {
        return $this->addTwoLists($l1, $l2, 0);
    }

    function addTwoLists(?ListNode $a, ?ListNode $b, int $add)
    {
        if ($a === null && $b === null && !$add) {
            return null;
        }
        $number = 0;
        $aNext = $bNext = null;
        if ($a !== null && $b !== null) {
            $number = $a->val + $b->val;
            $aNext = $a->next;
            $bNext = $b->next;
        } elseif ($a !== null) {
            $number = $a->val;
            $aNext = $a->next;
        } elseif ($b !== null) {
            $number = $b->val;
            $bNext = $b->next;
        }

        $number += $add;
        $add = 0;

        if ($number >= 10) {
            $add = 1;
            $number -= 10;
        }

        $list = new ListNode($number);
        $list->next = $this->addTwoLists($aNext, $bNext, $add);
        return $list;
    }
}